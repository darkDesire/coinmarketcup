import Dependencies._

lazy val backend = (project in file("backend"))
  .settings(
    name := "backend",
    version := "0.0.1",
    libraryDependencies ++= backendDeps
  )