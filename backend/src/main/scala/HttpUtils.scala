import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.HttpRequest
import akka.stream.Materializer

import scala.concurrent.{ExecutionContextExecutor, Future}
import scala.concurrent.duration._

trait HttpUtils {
  val timeout: FiniteDuration = 1.second

  def currencyLink(slug: String) =
    Option(System.getProperty("currencyLink"))
      .map(link => link.replace("$slug", slug))
      .getOrElse(throw new Exception("can't get currency link"))

  def asyncRequest(request: HttpRequest)(
    implicit system: ActorSystem,
    ec: ExecutionContextExecutor,
    mat: Materializer
  ): Future[Either[Throwable, String]] = {
    def future =
      (for {
        resp <- Http().singleRequest(request)
        strResp <- resp.entity.toStrict(timeout).map(_.data.utf8String)
      } yield Right(strResp)) recover {
        case th: Throwable => Left(th)
      }

    future
  }
}
