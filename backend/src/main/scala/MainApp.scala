import java.nio.file.Paths

import akka.actor.ActorSystem
import akka.http.scaladsl.model.{HttpMethods, HttpRequest}
import akka.stream.scaladsl.{FileIO, Flow, Keep, Sink, Source}
import akka.stream.{ActorMaterializer, IOResult, ThrottleMode}
import akka.util.ByteString
import io.circe.Printer
import io.circe.generic.auto._
import io.circe.parser.parse
import io.circe.syntax._
import net.ruippeixotog.scalascraper.browser.JsoupBrowser
import net.ruippeixotog.scalascraper.dsl.DSL.Extract._
import net.ruippeixotog.scalascraper.dsl.DSL._

import scala.concurrent.Future
import scala.concurrent.duration._

object MainApp extends App with HttpUtils {

  case class MarketItem(name: String,
                        rank: Int,
                        slug: String,
                        site: Option[String])

  implicit val system = ActorSystem("crawler")
  implicit val ec = system.dispatcher
  implicit val mat = ActorMaterializer()

  val link = Option(System.getProperty("link")).getOrElse(throw new Error("link is not defined"))

  def getAllCoinsSlug: Future[Either[Throwable, String]] =
    asyncRequest(HttpRequest(HttpMethods.GET, link))

  getAllCoinsSlug.foreach { resp =>
    val result = resp.fold(
      th => throw new Exception("something went wrong in parsing coins slugs", th),
      result => result
    )
    val list = parse(result)
      .flatMap(_.as[List[MarketItem]])
      .fold(
        er => throw new Exception("can't parse as List[CoinMarketItem]", er),
        list => list.sortBy(_.rank)
      )
    println(list.mkString("-->\n", "\n", "\n<--"))

    val browser = JsoupBrowser()

    val file = Option(System.getProperty("file"))
      .getOrElse(throw new Exception("output dir/file.ext not defined"))
    val fileSink: Sink[MarketItem, Future[IOResult]] =
      Flow[MarketItem]
        .map { s =>
          val newLine = s.asJson.pretty(Printer.noSpaces.copy(dropNullValues = true)) + "\n"
          print(newLine)
          ByteString(newLine)
        }
        .toMat(FileIO.toPath(Paths.get(file)))(Keep.right)

    def stream =
      Source(list.take(100))
        .throttle(10, 1.second, 0, ThrottleMode.Shaping)
        .map { item =>
          val doc = browser.get(currencyLink(item.slug)).body
          val details = doc >> element("div.details-panel")
          val allAs = details >> elementList("a")
          val sourceCodeLink =
            (allAs >/~ validator(text("a"))(_.contains("Source Code")))
              .collectFirst { case Right(elem) => elem.attr("href") }
          item.copy(site = sourceCodeLink)
        }
        .toMat(fileSink)(Keep.right)
        .run()

    stream.foreach(res => println(s"Stream done with status successful:${res.wasSuccessful}"))
  }
}
