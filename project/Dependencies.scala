import sbt._
object Dependencies {
  //Versions
  lazy val akkaVersion = "2.5.16"
  lazy val akkaHttpVersion = "10.1.4"
  lazy val circeVersion = "0.9.3"
  lazy val scalaScraper = "2.1.0"

  //Libraries
  ////akka
  val akkaActor = "com.typesafe.akka" %% "akka-actor" % akkaVersion
  val akkaStream = "com.typesafe.akka" %% "akka-stream" % akkaVersion
  val akkaHttp = "com.typesafe.akka" %% "akka-http" % akkaHttpVersion
  ////circe
  val circeCore = "io.circe" %% "circe-core" % circeVersion
  val circeGeneric = "io.circe" %% "circe-generic" % circeVersion
  val circeParser = "io.circe" %% "circe-parser" % circeVersion
  ////scrapper
  val scrapper = "net.ruippeixotog" %% "scala-scraper" % scalaScraper

  //Deps
  val akka = Seq(akkaActor, akkaStream, akkaHttp)
  val circe = Seq(circeCore, circeGeneric, circeParser)

  val backendDeps = akka ++ circe ++ Seq(scrapper)
}
