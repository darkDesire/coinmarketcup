# Coinmarketcup crawler

Coinmarketcup crawler is a simple application, that uses:

- akka, akka-streams: for **backpressure** while parsing

- akka-http, scalascraper - for **parsing** web sites

- io-circe - for **JSON serializer**

### Flags for run:
 - -Dlink="value"
 - -DcurrencyLink="value"
 - -Dfile="value"

#### Flags description table

| Key | Value | Description
| ------ | ------ | ------ |
| link | https://s2.coinmarketcap.com/generated/search/quick_search.json | link for file that contains all slug |
| currencyLink | https://coinmarketcap.com/currencies/$slug/ | pattern-link for getting data about special currency by slug |
| file | C:\Users\USER\output | Set path/file.ext for output data |